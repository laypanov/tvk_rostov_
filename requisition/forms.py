from django import forms
from requisition.models import Requisition


class RequisitionForm(forms.ModelForm):
    class Meta:
        model = Requisition
        fields = ['no_sh', 'apartment', 'name', 'phone', 'street', 'house', 'status']
        widgets = dict(no_sh=forms.CheckboxInput(
            attrs={}), house=forms.HiddenInput(
            attrs={'placeholder': 'Номер дома'}), street=forms.HiddenInput(
            attrs={'placeholder': 'Название улицы'}), apartment=forms.TextInput(
            attrs={'placeholder': 'Номер квартиры', 'id': 'r_apartment', 'class': 'form_hide'}),
            name=forms.TextInput(
            attrs={'placeholder': 'Как вас зовут?', 'id': 'r_name', 'class': 'form_hide', 'required': ""}),
            phone=forms.TextInput(
            attrs={'placeholder': '+7(999)-999-99-99', 'id': 'r_phone', 'class': 'tel_number', 'required': ""}))
