from django.contrib import admin
from requisition.models import Requisition, Street, House


class RequisitionAdmin(admin.ModelAdmin):
    fields = []
    readonly_fields = [
        'name',
        'street',
        'house',
        'apartment',
        'no_sh',
        'phone',
        'status',
        'date']
    list_display = [
        'name',
        'street',
        'house',
        'apartment',
        'no_sh',
        'phone',
        'status',
        'date']


admin.site.register(Requisition, RequisitionAdmin)
admin.site.register(Street)
admin.site.register(House)
