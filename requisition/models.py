from django.db import models
from django.utils.translation import ugettext_lazy as _


class House(models.Model):
    number = models.CharField(_('Номер дома'), unique=True, max_length=10)

    def __str__(self):
        return self.number

    class Meta:
        verbose_name = _('Дом')
        verbose_name_plural = _('Дома')


class Street(models.Model):
    name = models.CharField(_('Улица'), unique=True, max_length=100)
    house = models.ManyToManyField(House, verbose_name=_('Номер дома'))

    class Meta:
        verbose_name = _('Улица')
        verbose_name_plural = _('Улицы')

    def __str__(self):
        return self.name


class Requisition(models.Model):
    name = models.CharField(_('Имя клиента'), max_length=100)
    street = models.CharField(_('Улица'), max_length=100, blank=True)
    house = models.CharField(_('Номер дома'), max_length=20, blank=True)
    apartment = models.PositiveSmallIntegerField(
        verbose_name=_('Номер квартиры'),
        max_length=5,
        null=True,
        blank=True, )
    no_sh = models.BooleanField(_('Нет адреса в списке'), default=False)
    phone = models.CharField(_('Номер телефона'), max_length=17)
    date = models.DateTimeField(_('Дата'), auto_now_add=True, blank=True)
    status = models.CharField(_('Статус'), max_length=20, blank=True)

    class Meta:
        verbose_name = _('Заявка')
        verbose_name_plural = _('Заявки')

    def __str__(self):
        return _('{0}, {1}, кв. {2}').format(
            self.street,
            self.status,
            self.house,
            self.apartment)
