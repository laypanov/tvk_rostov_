from django.shortcuts import render
from company.models import Contact, Payment
from requisition.models import Street
from tariff.models import Rate
from requisition.forms import RequisitionForm
from requisition.models import Street
from django.core.mail import send_mail
from django.contrib import messages
import datetime


def index_page(request):
    arr = {}
    arr["title"] = "Абонентское обслуживание"
    arr["contacts"] = Contact.objects.all()
    arr["time"] = datetime.datetime.now()
    return render(request, 'support/index.html', arr)


def contacts(request):
    arr = {}
    arr["title"] = "Контактная информация"
    arr["contacts"] = Contact.objects.all()
    arr["time"] = datetime.datetime.now()
    return render(request, 'support/contacts.html', arr)


def rate(request):
    arr = {}
    arr["title"] = "Тарифы"
    arr["rates"] = Rate.objects.all()
    arr["contacts"] = Contact.objects.all()
    arr["time"] = datetime.datetime.now()
    return render(request, 'support/rate.html', arr)


def zone(request):
    arr = {}
    arr["title"] = "Зона охвата"
    arr["contacts"] = Contact.objects.all()
    arr["r_streets"] = Street.objects.all()
    arr["time"] = datetime.datetime.now()
    return render(request, 'support/zone.html', arr)


def out_of_service(request):
    arr = {}
    arr["title"] = "Вызов специалиста"
    arr["requisitions"] = RequisitionForm()
    arr["contacts"] = Contact.objects.all()
    arr["r_streets"] = Street.objects.all().order_by("name")
    arr["time"] = datetime.datetime.now()
    return render(request, 'support/out_of_service.html', arr)


def out_of_service_post(request):
    if request.method == 'POST':
        form = RequisitionForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Ваша заявка была отправлена, ожидайте звонка от оператора')
            send_mail('Вызов специалиста',
                  'Телефон: ' + request.POST['phone'] + '\n' + 'Имя: ' + request.POST['name'] + '\n' +
                  'Улица: ' + request.POST['street'] + '\n' + 'Дом: ' + request.POST['house'] + '\n' +
                  'Квартира: ' + request.POST['apartment'] + 'Статус: ' + request.POST['status'] + '\n',
                  'mail@tvk-rostov.ru', ['mail@tvk-rostov.ru'])
        return out_of_service(request)


def pay(request):
    arr = {}
    arr["title"] = "Способы оплаты"
    arr["contacts"] = Contact.objects.all()
    arr["time"] = datetime.datetime.now()
    return render(request, 'support/pay.html', arr)


def sberbank(request):
    arr = {}
    arr["title"] = "Сбербанк"
    arr["contacts"] = Contact.objects.all()
    arr["pays"] = Payment.objects.all().filter(bank_beneficiary="Юго-Западный Банк ОАО «Сбербанк России»")
    arr["time"] = datetime.datetime.now()
    return render(request, 'support/sberbank.html', arr)


def centrinvest(request):
    arr = {}
    arr["title"] = "Центр-Инвест"
    arr["contacts"] = Contact.objects.all()
    arr["pays"] = Payment.objects.all().filter(bank_beneficiary="ОАО КБ «Центр-Инвест» в. г. Ростове -на-Дону")
    arr["time"] = datetime.datetime.now()
    return render(request, 'support/centrinvest.html', arr)


def contract(request):
    arr = {}
    arr["title"] = "Действия с договором"
    arr["contacts"] = Contact.objects.all()
    arr["time"] = datetime.datetime.now()
    arr["time"] = datetime.datetime.now()
    return render(request, 'support/contract.html', arr)


def renewal(request):
    arr = {}
    arr["title"] = "Возобновление договора"
    arr["contacts"] = Contact.objects.all()
    arr["time"] = datetime.datetime.now()
    return render(request, 'support/renewal.html', arr)


def suspension(request):
    arr = {}
    arr["title"] = "Приостановление договора"
    arr["contacts"] = Contact.objects.all()
    arr["time"] = datetime.datetime.now()
    return render(request, 'support/suspension.html', arr)


def reregistration(request):
    arr = {}
    arr["title"] = "Переоформление договора"
    arr["contacts"] = Contact.objects.all()
    arr["time"] = datetime.datetime.now()
    return render(request, 'support/reregistration.html', arr)


def recalculation(request):
    arr = {}
    arr["title"] = "Переоформление договора"
    arr["contacts"] = Contact.objects.all()
    arr["time"] = datetime.datetime.now()
    return render(request, 'support/recalculation.html', arr)


def moving(request):
    arr = {}
    arr["title"] = "Переезд на другой адрес"
    arr["contacts"] = Contact.objects.all()
    arr["time"] = datetime.datetime.now()
    return render(request, 'support/moving.html', arr)


def termination(request):
    arr = {}
    arr["title"] = "Расторжение договора"
    arr["contacts"] = Contact.objects.all()
    arr["time"] = datetime.datetime.now()
    return render(request, 'support/termination.html', arr)


def adhrw(request):
    arr = {}
    arr["contacts"] = Contact.objects.all()
    arr["time"] = datetime.datetime.now()
    arr["title"] = "Настрйока приставки"
    return render(request, 'support/adhrw.html', arr)


def conf_tv(request):
    arr = {}
    arr["contacts"] = Contact.objects.all()
    arr["time"] = datetime.datetime.now()
    arr["title"] = "Настрйока ТВ"
    return render(request, 'support/conf_tv.html', arr)