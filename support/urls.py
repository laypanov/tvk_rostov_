from django.conf.urls import patterns, include, url
from tvk_rostov import views

urlpatterns = patterns('support.views',
                       url(r'^$', 'index_page', name="support"),
                       url(r'^contacts/$', 'contacts', name="contacts"),
                       url(r'^rate/$', 'rate', name="rate"),
                       url(r'^zone/$', 'zone', name="zone"),
                       url(r'^out_of_service/$', 'out_of_service', name="out_of_service"),
                       url(r'^out_of_service_post/$', 'out_of_service_post', name="out_of_service_post"),
                       url(r'^pay/$', 'pay', name="pay"),
                       url(r'^pay/sberbank/$', 'sberbank', name="sberbank"),
                       url(r'^pay/centrinvest/$', 'centrinvest', name="centrinvest"),
                       url(r'^contract/$', 'contract', name="contract"),
                       url(r'^contract/renewal/$', 'renewal', name="renewal"),
                       url(r'^contract/suspension/$', 'suspension', name="suspension"),
                       url(r'^contract/reregistration/$', 'reregistration', name="reregistration"),
                       url(r'^contract/recalculation/$', 'recalculation', name="recalculation"),
                       url(r'^contract/termination/$', 'termination', name="termination"),
                       url(r'^contract/moving/$', 'moving', name="moving"),
                       url(r'^adhrw/$', 'adhrw', name="adhrw"),
                       url(r'^conf_tv/$', 'conf_tv', name="conf_tv"),
)
