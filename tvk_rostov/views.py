from django.shortcuts import render
from django.shortcuts import redirect
from cable_tv.models import Theme
from requisition.forms import RequisitionForm
from requisition.models import Street
from sale.models import Sale
from other.models import Other
from company.models import Contact
from django.http import JsonResponse
from django.core.mail import send_mail
from django.contrib import messages
from django.db.models import Max
import json
import datetime


def index_page(request):
    arr = {}
    arr["title"] = "ТВК - ваше кабельное телевиденье"
    arr["themes"] = Theme.objects.all()
    arr["requisitions"] = RequisitionForm()
    arr["sales"] = Sale.objects.all()
    arr["other"] = Other.objects.all()
    arr["contacts"] = Contact.objects.all()
    arr["r_streets"] = Street.objects.all().order_by("name")
    arr["time"] = datetime.datetime.now()
    return render(request, 'main.html', arr)


def requisition_post(request):
    if request.method == 'POST':
        form = RequisitionForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Ваша заявка была отправлена, ожидайте звонка от оператора')
            if 'no_sh' in request.POST:
                no_sh = "Нету в списке"
            else:
                no_sh = "Есть в списке"
            send_mail('Новая заявка',
                      'Телефон: ' + request.POST['phone'] + '\n' + 'Имя: ' + request.POST['name'] + '\n' +
                      'Улица: ' + request.POST['street'] + '\n' + 'Дом: ' + request.POST['house'] + '\n' +
                      'Квартира: ' + request.POST['apartment'] + '\n' + 'Адреса нет в списке?: ' + no_sh +
                      'Статус: ' + request.POST['status'] + '\n',
                      'mail@tvk-rostov.ru', ['mail@tvk-rostov.ru'])
        return redirect('/')


def street_home(request):
    if request.method == 'GET':
        id_srt = int(request.GET['id'])
        street = Street.objects.get(id=id_srt)
        arr = {}
        arr['street_home'] = street.house.values('number')
        return render(request, 'street_home.html', arr)


def back_img(request):
    if request.method == 'GET':
        img = Other.objects.all().values()
        json_subcat = list(img)
        return JsonResponse(json.dumps(json_subcat), safe=False)


def add_chanals(request):
    if request.method == 'GET':
        first = int(request.GET['count']) - 1
        """last = first + 1"""
        arr = {}
        arr["chanels"] = Channel.objects.all().annotate(channel_number=Max('number')).order_by('channel_number')[
                         first:]
        return render(request, 'add_chanals.html', arr)
