from django.conf.urls import patterns, include, url
from django.core.urlresolvers import reverse
from django.conf import settings
from django.contrib import admin
from tvk_rostov import views


urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.index_page, name="main_index"),
    url(r'^company/', include('company.urls')),
    url(r'^support/', include('support.urls')),
    url(r'^channels/', include('cable_tv.urls')),
    url(r'^requisition_post/$', views.requisition_post, name="requisition_post"),
    url(r'ajax/street_home/', views.street_home),
    url(r'^ajax/add_chanals/', views.add_chanals),
    url(r'ajax/back_img/', views.back_img),
)

if settings.DEBUG:
    urlpatterns += patterns(
        '',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT,}),
        )
