$(document).ready(function() {
    var last_head_li = 0;
    var map;
    var i_last = 1;
    var pathname = window.location.pathname;
    var top = $(document).scrollTop();
    var b_img_c = [];
    var b_pic_no = 0;
    var add_now = 0;
    var doc_h = 0;


    doc_h = $(document).height();
    footer_top = $(".footer").offset().top;
    footer_h = $(".footer").height();
    fr = doc_h - footer_h - footer_top;
    $(".footer").css("margin-top", fr+49);


    if(pathname == "/channels/digital_dvb_t2/" || pathname == "/channels/digital_dvb_c/" || pathname == "/channels/analog/")
        $("#t_chanels").tablesorter({sortList: [[0,0]]});

    $(".tel_number").inputmask("+7(999)-999-99-99");
    get_background();   

    if(pathname == "/support/zone/" || pathname == "/support/contacts/")
        DG.then(function() {
            // загрузка кода модуля
            return DG.plugin('http://2gis.github.io/mapsapi/vendors/Leaflet.markerCluster/leaflet.markercluster-src.js');
        }).then(function() {
            if(pathname == "/support/zone/")
                map = DG.map('map', {
                    center: DG.latLng(47.24, 39.71),
                    zoom: 12
                });
            else{
                map = DG.map('map', {
                    center: DG.latLng(47.247952, 39.714436),
                    zoom: 16
                });
            }
            map.scrollWheelZoom.disable();
            var markers = DG.markerClusterGroup();
            // обработка координат для установки маркеров
            for (var i = 0; i < addressPoints.length; i++) {
                var a = addressPoints[i];
                var title = a[2];
                var marker = DG.marker([a[0], a[1]], { title: title });
                marker.bindPopup(title);
                markers.addLayer(marker);
            }
            map.addLayer(markers);
        });

    $(window).scroll(function() {
        top = $(document).scrollTop();
        if (top > 69) $(".head_menu").addClass( "scroll" );
        else $(".head_menu").removeClass("scroll");
    });

    $('.head_menu ul li a').click(function() {
        switch (true) {
            case $(this).hasClass('hc'):
                sctel(".connect");
                return false;
            case $(this).hasClass('hch'):
                sctel(".tv_chan");
                return false;
        }
    });

    function sctel (el) {
        top = $(document).scrollTop();
        if(top == 0) top = -49; else top = 0;
        $('html,body').stop().animate({ scrollTop: $(el).offset().top - 60 + top }, 1000);
    }

    $("#but_fi").click(function() {
        i_last = 1;
        var ch_name = $("#fi_1").val();
        var chbox_quality = $("#fi_quality option:selected" ).text();
        var theme_select = $("#fi_theme option:selected" ).text();
        var size_table = $("#chanals table tr").length;
        theme_select = $.trim(theme_select);
        ch_name = $.trim(ch_name);

        function check_find(one,two){
            size = two.length;
            for (var i=0; i<size; i++){
                t_two = $.trim(two[i]);
                if(one == t_two){
                    return true;
                }
            }
        }

        for (var i = 1; i <= size_table-1; i++){
            var tb_ch_name = $("#chanals tr:eq("+i+") td:eq(1)").text();
            tb_ch_name = $.trim(tb_ch_name);
            //var ch = ch_name.match(new RegExp(tb_ch_name+"$", "i")); alert(ch)
            var ch = -1 < tb_ch_name.toUpperCase().indexOf(ch_name.toUpperCase());
            var tb_theme_select = $("#chanals tr:eq("+i+") .theme").text().split(',');
            if ((ch || ch_name == "")&&
                (check_find(theme_select,tb_theme_select) || theme_select == "Любая тематика")){
                $("#chanals tr:eq("+i+")").show();
                i_last++;
            } else {
                $("#chanals tr:eq("+i+")").hide();
            }
            if (i_last % 2 === 0){
                $("#chanals tr:eq("+i+")").css("background", "rgba(226, 226, 226, 1)");
            }else{
                $("#chanals tr:eq("+i+")").css("background", "none");
            }
        }
    });

    $("select#street_sel").change(function(){
        $.get( "ajax/street_home/",{id:+$(this).val()}, function( data ) {
            $('#id_street').val($( "#street_sel option:selected" ).text());
            $('#house_sel').children().remove();
            var number = data.split(',');
            for (i=0; i<= number.length; i++){
                if(number[i]){
                    number[i] = $.trim(number[i]);
                    $('#house_sel')
                    .append($("<option></option>")
                            .remove()
                            .attr("value",i)
                            .text(number[i]));
                }
            }
            $('#id_house').val($( "#house_sel option:selected" ).text());
        });
    });

    $("#house_sel").change(function() {
        $("#id_house").val($("#house_sel option:selected").text());
    });

    $("#id_no_sh").change(function() {
        type_p = $("#id_house").attr("type");

        function get_inp(value1,value2){
            if(value1 == "hidden"){
                $('#id_street').val($( "#street_sel option:selected" ).text());
                $('#id_house').val($( "#house_sel option:selected" ).text());
            }else{
                $("#id_house").val("");
                $("#id_street").val("");
            }
            $("#id_house").attr("type", value1);
            $("#id_street").attr("type", value1);
            $("#street_sel").attr("type", value2);
            $("#house_sel").attr("type", value2);
        }

        if(type_p == "hidden"){
            get_inp("text", "hidden");
        }else{
            get_inp("hidden", "text");
        }
    });

    function get_background(){
        $.get( "ajax/back_img/",{}, function( data ) {
            obj = JSON.parse(data);

            for(var k in obj){
                b_img_c.push(obj[k].background_image);
            }
            switch( true ){
                case obj.length == 0:
                    b_pic_no = -1;
                    break;
                case obj.length > 1:
                    b_pic_no++;
                case true:
                    $("body").css("background-image", "url(/media/"+b_img_c[0]+")");
                    break;
            }
        });
    }

    setInterval(function(){
        if(b_pic_no != -1){
            $("body").css("background-image", "url(/media/"+b_img_c[b_pic_no]+")");
            if(b_pic_no < b_img_c.length-1) b_pic_no++; else b_pic_no = 0;
        }
    },30000);
});
