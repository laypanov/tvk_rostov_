from django.contrib import admin
from other.models import Other


class ImageAdmin(admin.ModelAdmin):
    list_display = ('image_img', 'background_image')

admin.site.register(Other, ImageAdmin)
