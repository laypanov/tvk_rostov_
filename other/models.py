from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError


class Other(models.Model):
    def validate_image(fieldfile_obj):
        filesize = fieldfile_obj.file.size
        megabyte_limit = 0.3
        if filesize > megabyte_limit*1024*1024:
            raise ValidationError("Максимальный размер файла %sMB" % str(megabyte_limit))

    background_image = models.ImageField(
        verbose_name=_('Путь к картинке'),
        help_text=_('Не более 300кб'),
        upload_to="pic/background",
        validators=[validate_image])

    class Meta:
        verbose_name = _('фоновые картинки')
        verbose_name_plural = _('Фоновая картинка')

    def image_img(self):
        if self.background_image:
            return u'<img src="%s" width="100"/>' % self.background_image.url
        else:
            return '(none)'
    image_img.short_description = 'Thumb'
    image_img.allow_tags = True