from django.conf.urls import patterns, include, url


urlpatterns = patterns('cable_tv.views',
                        url(r'^$', 'index_page', name="cable_tv"),
                        url(r'^digital_dvb_t2/$', 'digital_channel_dvb_t2', name="digital_channel_dvb_t2"),
                        url(r'^digital_dvb_c/$', 'digital_channel_dvb_c', name="digital_channel_dvb_c"),
                        url(r'^analog/$', 'analog_channel', name="analog_channel"),
)
