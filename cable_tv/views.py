from django.shortcuts import render
from company.models import Contact
from cable_tv.models import Channel, Theme, Chmode, Adinfo
from django.shortcuts import redirect
import datetime


def index_page(request):
    return redirect('analog_channel')


def analog_channel(request):
    arr = {}
    arr["title"] = "Аналоговые каналы"
    arr["contacts"] = Contact.objects.all()
    arr["themes"] = Theme.objects.all()
    arr["themes"] = Theme.objects.all()
    arr["chanels"] = Channel.objects.all().filter(da_mode=1)
    arr["adinfos"] = Adinfo.objects.all()
    return render(request, 'cable_tv/analog.html', arr)


def digital_channel_dvb_t2(request):
    arr = {}
    arr["title"] = "Цифровые каналы DVB-T2"
    arr["contacts"] = Contact.objects.all()
    arr["themes"] = Theme.objects.all()
    arr["chanels"] = Channel.objects.all().filter(da_mode=2)
    arr["time"] = datetime.datetime.now()
    arr["adinfos"] = Adinfo.objects.all()
    return render(request, 'cable_tv/digital_channel_dvb_t2.html', arr)


def digital_channel_dvb_c(request):
    arr = {}
    arr["title"] = "Цифровые каналы DVB-C"
    arr["contacts"] = Contact.objects.all()
    arr["themes"] = Theme.objects.all()
    arr["chanels"] = Channel.objects.all().filter(da_mode=3)
    arr["time"] = datetime.datetime.now()
    arr["adinfos"] = Adinfo.objects.all()
    return render(request, 'cable_tv/digital_channel_dvb_c.html', arr)