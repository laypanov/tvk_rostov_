from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError
from re import match, compile


class Theme(models.Model):
    channel_theme = models.CharField(_('Тематика канала'), max_length=100)

    class Meta:
        ordering = ('channel_theme', )
        verbose_name = _('Тематика канала')
        verbose_name_plural = _('Тематика каналов')

    def __str__(self):
        return self.channel_theme


class Chmode(models.Model):
    channel_mode = models.CharField(_('Тип сигнала'), max_length=100)

    class Meta:
        ordering = ('channel_mode', )
        verbose_name = _('Тип сигнала')
        verbose_name_plural = _('Тип сигнала')

    def __str__(self):
        return self.channel_mode


class Channel(models.Model):
    def validate_frequency(self):
        # regexp mean "three digits comma two digits"
        regexp = compile('\d{1,3}[,]\d{1,2}')
        if match(regexp, self) is None:
            raise ValidationError(
                message=_('Выражение не является частотой'),
                code='NotFrequency')

    number = models.PositiveSmallIntegerField(
        max_length=4,
        verbose_name=_('Номер канала'),
        help_text=_('Номер канала (цифрами)'))
    name = models.CharField(
        max_length=100,
        verbose_name=_('Название канала'),
        help_text=_('Полное название канала, например, Первый'))
    description = models.TextField(
        verbose_name=_('Описание'))
    da_mode = models.ManyToManyField(
        Chmode,
        verbose_name=_('Тип канала'),
        help_text=_('Цифровой, Аналоговый'))
    frequency = models.CommaSeparatedIntegerField(
        max_length=6,
        validators=[validate_frequency],
        verbose_name=_('Частота'),
        help_text=_('Например, 123,43'))
    url = models.URLField(
        max_length=200,
        verbose_name=_('URL'),
        help_text=_('Официальный сайт канала, например, 1tv.ru'))
    logo_image = models.ImageField(
        verbose_name=_('Логотип канала'),
        upload_to="pic/cable_tv/chanels")
    themes = models.ManyToManyField(
        Theme,
        verbose_name=_('Тематика канала'))

    class Meta:
        ordering = ('name', )
        verbose_name = _('Список каналов')
        verbose_name_plural = _('Списки каналов')

    def __str__(self):
        return self.name


class Adinfo(models.Model):
    name = models.CharField(_('Заголовок'), max_length=100)
    description = models.TextField(_('Описание'))
    date_start = models.DateField(_('Дата начала'))
    date_finish = models.DateField(_('Дата окончания'))

    class Meta:
        verbose_name = _('Дополнительная информация')
        verbose_name_plural = _('Дополнительная информация')

    def __str__(self):
        return self.name
