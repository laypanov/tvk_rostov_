from django.contrib import admin
from cable_tv.models import Channel, Theme, Chmode, Adinfo


class ChannelAdmin(admin.ModelAdmin):
    fields = []
    list_display = [
        'name',
        'number',
        'frequency',
        'description',
    ]

class AdinfoAdmin(admin.ModelAdmin):
    fields = []
    list_display = [
        'name',
        'description',
        'date_start',
        'date_finish',
    ]


admin.site.register(Adinfo, AdinfoAdmin)
admin.site.register(Channel, ChannelAdmin)
admin.site.register(Theme)
admin.site.register(Chmode)