from django.contrib import admin
from sale.models import Sale


class SaleAdmin(admin.ModelAdmin):
    fields = []
    list_display = [
        'name',
        'description',
        'date_start',
        'date_finish',
    ]


admin.site.register(Sale, SaleAdmin)
