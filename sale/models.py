from django.db import models
from django.utils.translation import ugettext_lazy as _


class Sale(models.Model):
    name = models.CharField(_('Название акции'), max_length=100)
    description = models.TextField(_('Описание'))
    date_start = models.DateField(_('Дата начала'))
    date_finish = models.DateField(_('Дата окончания'))

    class Meta:
        verbose_name = _('Акция')
        verbose_name_plural = _('Акции')

    def __str__(self):
        return self.name
