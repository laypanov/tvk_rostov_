from django.db import models
from django.utils.translation import ugettext_lazy as _


class Rate(models.Model):
    first = models.CharField(_('Первый взнос'), max_length=5)
    second = models.CharField(_('Второй взнос'), max_length=5)
    month = models.CharField(_('Ежемесячная плата'), max_length=5)

    def __str__(self):
        return self.first

    class Meta:
        verbose_name = _('Тариф')
        verbose_name_plural = _('Тарифы')
