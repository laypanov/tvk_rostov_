from django.contrib import admin
from tariff.models import Rate


class PayAmdin(admin.ModelAdmin):
    fields = []
    list_display = [
        'first',
        'second',
        'month']


admin.site.register(Rate, PayAmdin)