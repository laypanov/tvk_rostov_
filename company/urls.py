from django.conf.urls import patterns, include, url


urlpatterns = patterns('company.views',
                        url(r'^$', 'index_company', name="index_company"),
                        url(r'^license_page/$', 'license_page', name="license_page"),
                        url(r'^rules_page/$', 'rules_page', name="rules_page"),
)
