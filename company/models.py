from django.db import models
from django.utils.translation import ugettext_lazy as _


class Contact(models.Model):
    name_full = models.CharField(_('Полное название'), max_length=100)
    name_short = models.CharField(_('Аббревиатура'), max_length=10)
    address = models.CharField(_('Юридический адрес'), max_length=50)
    about = models.TextField(_('Пару слов о компании'))
    email = models.EmailField(_('Электронная почта'))
    phone = models.CharField(_('Контактный телефон'), max_length=17)

    class Meta:
        verbose_name = _('Информация о компании')
        verbose_name_plural = _('Информация о компании')

    def __str__(self):
        return self.name_short


class Payment(models.Model):
    receiver = models.CharField(_('Название компании (получателя)'), max_length=100)
    vat_id = models.CharField(_('ИНН компании'), max_length=10)
    account_settlement = models.CharField(_('Расчётный счёт'), max_length=20)
    bank_beneficiary = models.CharField(_('Название банка компании'), max_length=100)
    bic = models.CharField(_('БИК'), max_length=9)
    account_correspondent = models.CharField(_('Корреспондентский счёт'), max_length=20)

    class Meta:
        verbose_name = _('Квитанция')
        verbose_name_plural = _('Квитанция')

    def __str__(self):
        return self.receiver
