from django.shortcuts import render
from company.models import Contact
import datetime

def index_company(request):
    arr = {}
    arr["title"] = "Документы"
    arr["contacts"] = Contact.objects.all()
    arr["time"] = datetime.datetime.now()
    return render(request, 'company/index.html', arr)


def license_page(request):
    arr = {}
    arr["title"] = "Лицензия"
    arr["contacts"] = Contact.objects.all()
    arr["time"] = datetime.datetime.now()
    return render(request, 'company/license_page.html', arr)


def rules_page(request):
    arr = {}
    arr["title"] = "Правила"
    arr["contacts"] = Contact.objects.all()
    arr["time"] = datetime.datetime.now()
    return render(request, 'company/rules_page.html', arr)
