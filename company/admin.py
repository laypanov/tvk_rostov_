from django.contrib import admin
from company.models import Contact, Payment


class ContactAdmin(admin.ModelAdmin):
    fields = []
    list_display = [
        'name_full',
        'name_short',
        'address',
        'about',
        'email',
        'phone',
    ]


class PaymentAdmin(admin.ModelAdmin):
    fields = []
    list_display = [
        'receiver',
        'vat_id',
        'account_settlement',
        'bank_beneficiary',
        'bic',
        'account_correspondent',
    ]

admin.site.register(Contact, ContactAdmin)
admin.site.register(Payment, PaymentAdmin)
